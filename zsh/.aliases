#!/usr/bin/zsh

# file stuff
alias cp='cp -v'
alias mv='mv -v'
alias rmr='rm -r'
alias rmrf='rm -rf'
alias cpr='cp -r'
alias scp='scp -v'
alias t='tree -aCFh -I .git'

# ls stuff
if [ -z "$(uname -a | nice grep Linux)" ]; then
    alias l='ls -lhaF'
    alias la='ls -a'
else
    alias ll='ls --color=auto -l'
    if which exa &> /dev/null; then
        alias la='exa -abghlT --ignore-glob=".bzr|CVS|.git|.hg|.svn|node_modules|__pycache__" --git'
        alias l='exa -abghl --git'
    else
        alias la='ls --color=auto -a'
        alias l='ls --color=auto -aCFlh'
    fi
fi

# grep related
alias grep='grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,node_modules} -n'

# git
alias gits='git status -s'
alias gita='git add'
alias gitd='git diff'
alias gitg='git grep'
alias gitb='git branch'
alias gitch='git checkout'
alias gitbch='git checkout -b'
alias gitcout='git checkout --'
alias gitm='git merge'
alias gitl='git log --graph --show-signature --date=iso-local'
alias gitre='git reset'
alias gitsh='git push'
alias gitll='git pull --rebase'
alias gitssh='git push --set-upstream origin $(git branch --show-current)'
alias gitsave='git config credential.helper store'
alias gitbare='git config --bool core.bare true'
alias gitrm='git rm'
alias gitcl='git clone --recurse-submodules'
alias gitr='git rebase'

# for network tests
alias wget='wget -v'

# python
alias ipy='ipython3 --autocall 1 -i'
alias ipy3='ipython3 --autocall 1 -i'
alias ipy2='ipython2 --autocall 1 -i'

# for ccrypt
alias cpt='ccrypt -v'

# BashBunny
# alias sshbunny='sudo screen /dev/tty.usbmodemch000001 115200'
# export BUNNYPATH=/Volumes/BashBunny/

# xclip
alias xclip='xclip -selection c'


# docker
alias dr='docker'
alias dps='docker ps'
alias drmi='docker rmi'
alias dls='docker image ls'
alias dshell='docker run -it -v `pwd`:~/host --workdir ~/host --rm debian:stretch'
alias ashell='docker run -it -v `pwd`:~/host --workdir ~/host --rm alpine:3.9'

# docker-compose
alias dc='docker-compose'
alias dcb='docker-compose build'
alias dcu='docker-compose up'
alias dcr='docker-compose run'
alias dcd='docker-compose down'
alias dck='docker-compose kill'

# misc.
alias cl='clear; cat ~/dots/zsh/$(hostname) | lolcat'
# alias rsync='rsync -r -ah --progress'
alias src='unalias -a; source ~/.zshrc'
alias erc='e ~/.zshrc; source ~/.zshrc'
alias efunc='e ~/dots/zsh/.functions'
alias ealias='e ~/dots/zsh/.aliases'
alias base64='base64 -w 0'
alias cll='clear; l'
alias ctl='sudo systemctl'
alias de=deactivate
# alias startx=xinit # don't ask
alias top=htop
alias wh=which
alias p=ccat
alias q=exit
alias ccache='sudo bash -c "free && sync && echo 3 > /proc/sys/vm/drop_caches && free"'

# lolz
alias gimmemake='cp ~/dots/templates/Makefile ./ && touch requirements.txt'
alias gimmesolve='cp ~/dots/templates/solve ./'
alias gimmedenv='cp -r ~/dots/templates/denv ./'
alias onoz='less /var/log/errors.log'
alias plz='sudo $(fc -ln -1)'
alias wtf='dmesg | less'
alias gimme='touch'
alias yeet='sudo rm -rf'

alias ghci='docker run --rm -it -w /opt/pl -v $(pwd):/opt/pl -u $(id -u):$(id -g) haskell ghci'
alias haskell='docker run --rm -it -w /opt/pl -v $(pwd):/opt/pl -u $(id -u):$(id -g) haskell bash'

# Kube
alias kctl=kubectl
alias kctlf='kubectl apply -f'
alias mk=minikube
alias mkctl='minikube kubectl --'
